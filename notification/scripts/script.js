var app = angular.module("myModule",[]);

app.controller("firstController", function ($scope){
	$scope.message = "My Angular Script";
});

app.controller("dataController", function ($scope){
	var listemp = [
	{firstname: "abc(170123123)", lastname: "xyz", gender: "male", salary: 35000},
	{firstname: "fhj(254789654)", lastname: "asd", gender: "female", salary: 45000},
	{firstname: "aqwert(287452546)", lastname: "qwe", gender: "male", salary: 25000},
	{firstname: "kjhgfg(9875641258)", lastname: "rty", gender: "female", salary: 15000},
	{firstname: "tyuiop(36985214025)", lastname: "fgh", gender: "male", salary: 5000}
	
	];
	$scope.employees = listemp;
});