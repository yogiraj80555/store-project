<?php 
include('header.php');
require_once '../include/DbO.php';
session_start();
if(empty($_SESSION['type']))
{
	header("location:index.php");
}else
{
if($_SESSION['type'] == "gate")
{
	echo "..";
}else
{
	header("location:index.php");
}
}

?>
<style>
input[type=date]{
    width: 100%;
    padding: 12px 20px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
}

#notification-count{
	font-size: .70em;		
	color: black;
	font-weight:bold;
}
</style>
<body>
<h1 align="center"  id="text2" >Store Outward</h1>
<br />

<div class="container">

<a href="index.php" style="margin-left: 165px; font-family:Lobaster;" class="btn btn-success">Return</a>
<br />
<br />
<form class="form-horizontal" action="outedit_save.php" method="post" autocomplete="on">    
<?php
include('../dbcon.php');
$id=$_POST['selector'];

$N = count($id);
$db = new DbOperations();
if($N == 0)
{echo "Count is:".$N;
header("location:notific.php");
	?>
<script>
var btn = document.getElementById("submit")
btn.disabled = true;
</script>
<?php	
}
for($i=0; $i < $N; $i++)
{
$data = $db->getes($id[$i]);
while($row = mysql_fetch_array($data))
{ ?>
	<div class="thumbnail" style="margin:auto; width:600px;">
	<div style="margin-left: 70px; margin-top: 20px;">
	
		<div class="control-group">
		<label class="control-label" for="inputEmail" style="font-family:Lobaster; font-weight:bold; font-size:18px; color:blue;">Outward No</label>
		<div class="controls">
			<input  name="outward[]" required="required" step="number" type="number" style="font-family:Lobaster; font-weight:bold;" value="" />
		</div>
		</div>
		
		
		<div class="control-group">
		<label class="control-label" for="inputEmail" style="font-family:Arial; font-weight:bold; font-size:15px; color:blue;">Name of Receiver<span id="notification-count"><?php echo "(Companey/Org Name)";?></span></label>
		<div class="controls">
			<input  name="receiver[]" required="required" type="text" style="font-family:Lobaster; font-weight:bold;" value="" />
		</div>
		</div>
		
		<div class="control-group">
		<label class="control-label" for="inputEmail" style="font-family:Lobaster; font-weight:bold; font-size:18px; color:blue;">By Hand</label>
		<div class="controls">
		<select name="byhand[]" style="font-family:Lobaster; font-weight:bold;  "   required="required">
		<option value="null">Select</option>
		<option value="yes">yes</option>
		<option value="No">No</option>
		</select>
		</div>
		</div>

		
		
		<div class="control-group">
		<label class="control-label" for="inputEmail" style="font-family:Lobaster; font-weight:bold; font-size:18px; color:blue;">Vehical Number</label>
		<div class="controls">
			<input  name="vehical[]" type="text" style="font-family:Lobaster; font-weight:bold;" value="" />
		</div>
		</div>
		
		<div class="control-group">
		<label class="control-label" for="inputEmail" style="font-family:Lobaster; font-weight:bold; font-size:18px; color:blue;">Person/Driver Name</label>
		<div class="controls">
			<input  name="personname[]" required="required" type="text" style="font-family:Lobaster; font-weight:bold;" value="" />
		</div>
		</div>
		
		
		<div class="control-group">
		<label class="control-label" for="inputEmail" style="font-family:Lobaster; font-weight:bold; font-size:18px; color:blue;">Challen Number</label>
		<div class="controls">
			<input  name="challen[]" type="text" style="font-family:Lobaster; font-weight:bold;" value="<?php echo $row['formno']; ?>" />
		</div>
		</div>
		
		
		<div class="control-group">
		<label class="control-label" for="inputEmail" style="font-family:Lobaster; font-weight:bold; font-size:18px; color:blue;">Discription</label>
		<div class="controls">
			<textarea required="required" name="description[]"  type="range" style="font-family:Lobaster; font-weight:bold; font-size:14px; color:red; " placeholder="Extra Info"></textarea>
		</div>
		</div>
		
		</br>
		</br>
		
		<div class="control-group">
		<label class="control-label" for="inputEmail" style="font-family:Lobaster; font-weight:bold; font-size:18px; color:red;">Material Details</label>
		<label class="control-label" for="inputEmail" style="font-family:Lobaster; font-weight:bold; font-size:18px; color:silver;">----------------------------------------------</label>
		</div>
		
		
		
		<input name="id[]" type="hidden" value="<?php echo  $row['id'] ?>" />
		
		
		<div class="control-group">
		<label class="control-label" for="inputEmail" style="font-family:Lobaster; font-weight:bold; font-size:18px; color:blue;">Item Name</label>
		<div class="controls">
			<input readonly="readonly" name="item[]" type="textarea" style="font-family:Lobaster; font-weight:bold; font-size:18px; color:black; " value="<?php echo "".$row['item'] ?> "/>
		</div>
		</div>
	
		<div class="control-group">
		<label class="control-label" for="inputEmail" style="font-family:Lobaster; font-weight:bold; font-size:18px; color:blue;"> Quantity</label>
		<div class="controls">
			<input  name="count[]" readonly="readonly" required="required" step="number" type="number" style="font-family:Lobaster; font-weight:bold;" value="<?php echo $row['count']?>" />
		</div>
		</div>
		
		
		
		<div class="control-group">
		<label class="control-label" for="date" style="font-family:Lobaster; font-weight:bold; font-size:18px; color:blue;">Date</label>
		<div class="controls">
			<input name="date[]" required="required" type="date" style="font-family:Lobaster; font-weight:bold;" value="" />
		</div>
		</div>
		
		
		<div class="control-group">
		<label class="control-label" for="date" style="font-family:Lobaster; font-weight:bold; font-size:18px; color:blue;">Time</label>
		<div class="controls">
			<input name="time[]" required="required" type="time" style="font-family:Lobaster; font-weight:bold;" placeholder="HH:MM" />
		</div>
		</div>
		<hr/>
		<div class="control-group">
		<label class="control-label" for="inputEmail" style="font-family:Lobaster; font-weight:bold; font-size:18px; color:blue;">Security Name</label>
		<div class="controls">
			<input  name="security[]" required="required" type="text" style="font-family:Lobaster; font-weight:bold;" value="" />
		</div>
		</div>
		
		
		
	</div>
	</div>

	<br />	
<?php 
}
}
?>
<input name="" id="submit" class="btn btn-success" style="margin-left: 165px; font-family:Lobaster;" type="submit" value="Update">

</form>

</div>
</body>
</html>