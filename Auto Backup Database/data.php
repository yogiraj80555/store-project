<?php
sleep(1);		
				include('xpage.php');
		
  ?>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="refresh" content="10">
	
<title>Do Not Close This Page</title>
    <style type="text/css">
        
        *{
            margin: 0;
            padding: 0;
        }
        body{
            background-color: #fff;
        }
        .fly-in-text{
            list-style: none;
            position: absolute;
            left: 50%;
            top:50%;
            transform: translateX(-50%) translateY(-50%);
        }
        .fly-in-text li{
            display: inline-block;
            margin-right: 50px;
            font-family: Open sans, sans-serif;
            font-weight: 300;
            font-size: 4em;
            color:#000;
            opacity: 1;
            transition: all 3.5s ease;
        }
        .fly-in-text li:last-child{
            margin-right: 0;
        }
        .fly-in-text.hidden li {
            opacity: 0.01;
        }
        
        .fly-in-text.hidden li:nth-child(1){ transform: translateX(-200px) translateY(-200px);}
        .fly-in-text.hidden li:nth-child(2){ transform: translateX(-300px) translateY(2250px);}
        .fly-in-text.hidden li:nth-child(3){ transform: translateX(-350px) translateY(-320px);}
        .fly-in-text.hidden li:nth-child(4){ transform: translateX(10px) translateY(-10px);}
        .fly-in-text.hidden li:nth-child(5){ transform: translateX(-300px) translateY(-400px);}
        .fly-in-text.hidden li:nth-child(6){ transform: translateX(290px) translateY(-290px);}
        .fly-in-text.hidden li:nth-child(7){ transform: translateX(-300px) translateY(-4000px);}
		.fly-in-text.hidden li:nth-child(8){ transform: translateX(-10px) translateY(10px);}
        
    </style>
    </head>
    <body>
        
        <ul class="fly-in-text hidden">
        
            <li>W</li>
            <li>E</li>
            <li>L</li>
            <li>C</li>
            <li>O</li>
            <li>M</li>
            <li>E</li>
			<li><?phpecho date("h:i:sa");?></li>
            
        
        </ul>
        <script src="jquery.min.js"></script>
        <script type="text/javascript">
        $(function(){
            setTimeout(function(){
                $('.fly-in-text').removeClass('hidden')
            },1000);
        })();
        </script>
        <?php
		echo "Time is: ".date("h:i:sa");
		
		?>
    </body>
	</html>