<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Store</title>


<link href="css/style.css" rel="stylesheet" type="text/css">
<style>
body{ background:url(img/back2.jpg);}
table {
    border-collapse: collapse;
    width: 80%;
	margin:auto;
	color:#FFFFFF;
	
	
}

th, td {
    text-align: center;
    padding: 8px;
	font-size:24px;
}
th {
    background-color: #4CAF50;
    color: white;
	
}
tr:nth-child(even){background-color: #f2f2f2}
tr:nth-child(odd){background-color:#FFFFFF};

</style>
</head>

<body>
<?php
session_start();
if($_SESSION['type']== "gate")
{
$_SESSION['cast']=1;
include('menu.php');
header('location:update/plmb.php');
}
if($_SESSION['type']== "store")
{
$_SESSION['cast']=11;
include('menu.php');
header('location:storedata/');
}
if(empty($_SESSION['type']))
{
	header('location:index.php');
}
?>

</body>
</html>
