<?php $plumb=0; $cln=0; $com=0; $ele=0; $mans=0; $ots=0; $prints=0; $stain=0;
include('../report/header.php');
require_once '../include/DbOx.php';
$db = new DbOperations();   
$data = $db->plumbingss();
$data1 = $db->clean();
$data2 = $db->comp();
$data3 = $db->elec();
$data4 = $db->manualss();
$data5 = $db->oths();
$data6 = $db->prints();
$data7 = $db->stck();
while($row=mysql_fetch_array($data)){$plumb = $plumb+1;}while($row=mysql_fetch_array($data1)){$cln = $cln+1;}while($row=mysql_fetch_array($data2)){$com = $com+1;}while($row=mysql_fetch_array($data3)){$ele = $ele+1;}while($row=mysql_fetch_array($data5)){$ots = $ots+1;}while($row=mysql_fetch_array($data4)){$mans = $mans+1;}while($row=mysql_fetch_array($data6)){$prints = $prints+1;}while($row=mysql_fetch_array($data7)){$stain = $stain+1;}
?><html>
<head>
<title>My First chart using FusionCharts its XML Data Suite XT</title>
<script type="text/javascript" src="js/fusioncharts.js"></script>
<script type="text/javascript" src="js/themes/fusioncharts.theme.fint.js"></script>
<script type="text/javascript">
FusionCharts.ready(function () {
    var myChart = new FusionCharts({
      "type": "column2d",
      "renderAt": "chartContainer",
      "width": "1000",
      "height": "680",
      "dataFormat": "xml",
      "dataSource": "<chart caption='Store Data Analysis' subcaption='Monthly revenue ' xaxisname='Material Name' yaxisname='Amount' numberprefix='#' palettecolors='#008aa4' bgalpha='1' borderalpha='20' canvasborderalpha='0' theme='fint' useplotgradientcolor='0' plotborderalpha='10' placevaluesinside='1' rotatevalues='1' valuefontcolor='#ffffff' captionpadding='20' showaxislines='1' axislinealpha='25' divlinealpha='10'><set label='Plumbing Material' value='<?php echo $plumb ?>' /><set label='Cleaning Material' value='<?php echo $cln ?>' /><set label='Computer Material' value='<?php echo $com ?>' /><set label='Electrical Material' value='<?php echo $ele ?>' /><set label='Manual' value='<?php echo $mans ?>' /><set label='Other Material' value='<?php echo $ots ?>' /><set label='Printing Stationary' value='<?php echo $prints ?>' /><set label='Stationary Stock' value='<?php echo $stain ?>' />"
    });

  myChart.render();
});
</script>
</head>
<body>
  <div id="chartContainer">Data Analysis</div>
</body>
</html>