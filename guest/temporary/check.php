<?php
session_start();
if(empty($_SESSION['type']))
{
	header("location:index.php");
}
else
{
	
	if($_SESSION['type'] == "gate")
	{
		header("location:gate2.php");
	}
	if($_SESSION['type'] == "store")
	{
		header("location:sstart.php");
	}
	if($_SESSION['type'] == "admin")
	{
		header("location:admin/index.php");
	}
	if($_SESSION['type'] == "guest")
	{
	    header("location:guest/index.php");
	}
}

?>