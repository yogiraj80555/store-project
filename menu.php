<!DOCTYPE html>
<html>
<head>
<style>
body {margin:auto;}
ul.topnav {
width:80%;
border-radius:8px;
  list-style-type: none;
  margin: auto;
  padding: 0;
  overflow: hidden;
  background-color: #333;
  margin-bottom:5px;
}

ul.topnav li {float: left;}

ul.topnav li a {
  display: inline-block;
  color: #f2f2f2;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
  transition: 0.3s;
  font-size: 17px;
}

ul.topnav li a:hover {background-color: #555;}

ul.topnav li.icon {display: none;}
.sidenav {
    height: 100%;
    width: 0;
    position: fixed;
    z-index: 1;
    top: 0;
    left: 0;
    background-color: #111;
    overflow-x: hidden;
    transition: 0.5s;
    padding-top: 60px;
}

.sidenav a {
    padding: 8px 8px 8px 32px;
    text-decoration: none;
    font-size: 25px;
    color: #818181;
    display: block;
    transition: 0.3s;
}

.sidenav a:hover, .offcanvas a:focus{
    color: #f1f1f1;
}

.sidenav .closebtn {
    position: absolute;
    top: 0;
    right: 25px;
    font-size: 36px;
    margin-left: 50px;
}

@media screen and (max-height: 450px) {
  .sidenav {padding-top: 15px;}
  .sidenav a {font-size: 18px;}
}
@media screen and (max-width:680px) {
ul.topnav{width:100%;}
  ul.topnav li:not(:first-child) {display: none;}
  ul.topnav li.icon {
    float: right;
    display: inline-block;
  }
}

@media screen and (max-width:680px) {
  ul.topnav.responsive {position: relative;}
  ul.topnav.responsive li.icon {
    position: absolute;
    right: 0;
    top: 0;
  }
  ul.topnav.responsive li {
    float: none;
    display: inline;
  }
  ul.topnav.responsive li a {
    display: block;
    text-align: left;
  }
}
</style>
</head>
<body >
<center><div><img src="img/head1.jpg" width="79%" height="150" /></div></center>
<div id="mySidenav" class="sidenav">
  <a href="javascript:void(0)" class="closebtn" onClick="closeNav()">&times;</a>
  <h2 align="center"><font color="#FFFFFF">Store Menu</font></h2>
  <a href="plumbing.php">Plumbing Material </a>
  <a href="Electrical.php">Electrical Material</a>
  <a href="Cleaning.php">Cleaning Material</a>
  <a href="Stationary.php">Stationary Stock</a>
  <a href="printing.php">Printing Stationary</a>
  <a href="computer.php">Computer Material</a>
</div>



<ul class="topnav" id="myTopnav">

  <li>
<span style="font-size:30px;cursor:pointer; color:#FFFFFF; margin-left:10px" onClick="openNav()">&#9776; </span></li>
  <li><a class="active" href="gate2.php">Home</a></li>
  <li><a href="about.php">About</a></li>
  <li><a href="#">Contact</a></li>
  
  
  <li class="icon">
    <a href="javascript:void(0);" style="font-size:15px;" onClick="myFunction()">+</a>
  </li>
  <li><a href="logout.php"><em><strong>Logout</strong></em></a></li>
</ul>


<script>
function myFunction() {
    var x = document.getElementById("myTopnav");
    if (x.className === "topnav") {
        x.className += " responsive";
    } else {
        x.className = "topnav";
    }
}
</script>

<script>
function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}
</script>

</body>
</html>
