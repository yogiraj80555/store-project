<?php 
include('header.php');
session_start();
if(empty($_SESSION['type']))
{
	header("location:../index.php");
}
if(!$_SESSION['type'] == "admin")
{
	header("location:../index.php");
}
?>
<style>
input[type=text]{
   
	width: 40%;
    padding: 3px 5px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
}
select{
   
	width: 40%;
    padding: 3px 5px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
}

#text2
{

background: linear-gradient(to bottom, rgba(176,223,242,1) 0%, rgba(20,196,243,1) 50%, rgba(186,228,243,1) 100%);
margin-left:auto;
margin-right:auto;
width:320px;
height:45px;
text-align:center;
font-size:29px;
color:#330033;
padding:6px;
border:solid #336699 1px ;
border-radius:5px 34px 5px 34px;
}
</style>
<body>

<br />

<div class="container">
<h1 id="text2">Temp Outward</h1> 
<a href="home.php" style="margin-left: 165px; font-family:Lobaster;" class="btn btn-success">Return</a>
<br />
<br />
<form class="form-horizontal" action="edit_saveout.php" method="post">    
<?php
$type = $_SESSION['type'];
$ids=true;	
while($ids)
{ ?>
	<div  style="margin:auto; width:600px; backgorund-color:rgba(255,0,0,0.4);">
	<div style="margin-left: 50px; margin-top: 10px;">
		<div class="control-group">
		<label class="control-label" for="inputEmail" style="font-family:Lobaster; font-weight:bold; font-size:18px; color:blue;">Outward No</label>
		<div class="controls">
			<input name="outward" type="text" required="required" style="font-family:Lobaster; font-weight:bold;" value="" />
		</div>
		</div>
		
		<div class="control-group">
		<label class="control-label" for="inputEmail" style="font-family:Lobaster; font-weight:bold; font-size:18px; color:blue;">Receiver Name</label>
		<div class="controls">
			<input name="receiver" type="text" required="required" style="font-family:Lobaster; font-weight:bold;" value="" />
		</div>
		</div>
		
		<div class="control-group">
		<label class="control-label" for="inputEmail" style="font-family:Lobaster; font-weight:bold; font-size:18px; color:blue;">By Hand</label>
		<div class="controls">
		<select name="byhand" style="font-family:Lobaster; font-weight:bold;" >
		<option value="No">No</option>
		<option value="yes">yes</option>
		</select>
		</div>
		</div>
		
		
		<div class="control-group">
		<label class="control-label" for="inputEmail" style="font-family:Lobaster; font-weight:bold; font-size:18px; color:blue;">Vehical Number</label>
		<div class="controls">
			<input  name="vehical" type="text" style="font-family:Lobaster; font-weight:bold;" value="" />
		</div>
		</div>
		
		<div class="control-group">
		<label class="control-label" for="inputEmail" style="font-family:Lobaster; font-weight:bold; font-size:18px; color:blue;">Person/Driver Name</label>
		<div class="controls">
			<input  name="personname" type="text" style="font-family:Lobaster; font-weight:bold;" value="" />
		</div>
		</div>
		
		
		<div class="control-group">
		<label class="control-label" for="inputEmail" style="font-family:Lobaster; font-weight:bold; font-size:18px; color:blue;">Challen Number</label>
		<div class="controls">
			<input  name="challen" type="text" style="font-family:Lobaster; font-weight:bold;" value="" />
		</div>
		</div>
		
		
		<div class="control-group">
		<label class="control-label" for="inputEmail" style="font-family:Lobaster; font-weight:bold; font-size:18px; color:blue;">Discription</label>
		<div class="controls">
			<textarea required="required" name="description"  type="range" style="font-family:Lobaster; font-weight:bold; font-size:14px; color:red; " placeholder="Extra Info"></textarea>
		</div>
		</div>
		</br>
		</br>
		
		<div class="control-group">
		<label class="control-label" for="inputEmail" style="font-family:Lobaster; font-weight:bold; font-size:18px; color:red;">Material Details</label>
		<hr/>
		</div>
		
		
		
	
		<div class="control-group">
		<label class="control-label" for="inputEmail" style="font-family:Lobaster; font-weight:bold; font-size:18px; color:blue;">Companey</label>
		<div class="controls">
			<input name="companey" type="text" style="font-family:Lobaster; font-weight:bold;" value="<?php  ?>" />
		</div>
		</div>
		
		<div class="control-group">
		<label class="control-label" for="inputEmail" style="font-family:Lobaster; font-weight:bold; font-size:18px; color:blue;">Item Name</label>
		<div class="controls">
			<textarea required="required" name="item"  type="range" style="font-family:Lobaster; font-weight:bold; font-size:14px; color:red; width:40%" placeholder="Item Names..."></textarea>
		</div>
		</div>
	
		<div class="control-group">
		<label class="control-label" for="inputEmail" style="font-family:Lobaster; font-weight:bold; font-size:18px; color:blue;">Quantity</label>
		<div class="controls">
			<input name="count" type="text" style="font-family:Lobaster; font-weight:bold;" value="<?php  ?>" />
		</div>
		</div>
		
		
		<div class="control-group">
		<label class="control-label" for="date" style="font-family:Lobaster; font-weight:bold; font-size:18px; color:blue;">Date</label>
		<div class="controls">
			<input name="date" required="required" type="date" style="font-family:Lobaster; font-weight:bold;" value="" />
		</div>
		</div>
		
		
		<div class="control-group">
		<label class="control-label" for="date" style="font-family:Lobaster; font-weight:bold; font-size:18px; color:blue;">Time</label>
		<div class="controls">
			<input name="time" required="required" type="time" style="font-family:Lobaster; font-weight:bold;" placeholder="HH:MM" />
		</div>
		</div>
		<hr/>
		
		<div class="control-group">
		<label class="control-label" for="date" style="font-family:Lobaster; font-weight:bold; font-size:18px; color:blue;">Security Name</label>
		<div class="controls">
			<input name="security" required="required" type="text" style="font-family:Lobaster; font-weight:bold;" value="" />
		</div>
		</div>
		
	
		
	</div>
	</div>

	<br />	
<?php 
$ids=false;}

?>
<input name="" class="btn btn-success" style="margin-left: 165px; font-family:Lobaster;" type="submit" value="Update">

</form>

</div>
</body>
</html>