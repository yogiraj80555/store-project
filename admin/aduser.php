 <?php
include('header.php');
session_start();

if(empty($_SESSION['type']))
{
	header("location:../index.php");
}
if(!$_SESSION['type'] == "admin")
{
	header("location:../index.php");
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Store</title>
<style>
 body{
            background-color: #001F1F;
        }

.button {
    background-color: #4CAF50; /* Green */
    border: none;
    color: white;
    padding: 16px 32px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    margin: 4px 2px;
    -webkit-transition-duration: 0.4s; /* Safari */
    transition-duration: 0.4s;
    cursor: pointer;
	border-radius: 6px;
	width:300px;
}

.button1 {
    background-color: white; 
    color: black; 
    border: 2px solid #4CAF50;
}

.button1:hover {
    background-color: #4CAF50;
    color: white;
	font-style: oblique;
	font-variant:small-caps;
}

.button2 {
    background-color: white; 
    color: black; 
    border: 2px solid #008CBA;
}

.button2:hover {
    background-color: #008CBA;
    color: white;
	font-style: oblique;
	font-variant:small-caps;
}

.contain
{
margin-top:3%;
}

#welcome{
	border: dotted gray 9px;
	width: auto;
	border-radius: 5px;
	margin: 100px auto;
	background: white;		
	}
</style>
</head>

<body background="img/back2.jpg">

<div align="center" class="contain">

<div class="container">
<div class="alert alert-info">
		
			<h2 style="text-align:center; font-family:Lobaster;">All Users
			</h2>
			<button  class="btn btn-success" style="margin:10px 0% auto; font-family:Lobaster;"  ><a  href="index.php" style="color:#FFFFFF; text-decoration:none">Home</a></button>
			
		</div></div>

</br>

<form class="form-horizontal" action="ading.php" method="post"> 


<div class="thumbnail" style="margin:auto; width:600px; border: dotted gray 9px; background: white; border-radius: 5px; margin: 50px auto;">

	<div style="margin-left: 70px; margin-top: 20px;">
		<div class="control-group">
		<label class="control-label" for="inputEmail" style="font-family:Lobaster; font-weight:bold; font-size:18px; color:blue;">User Name</label>
		<div class="controls">
		<input name="id" type="hidden" value="<?php  ?>" />
			<input name="username" required="required" type="text" style="font-family:Lobaster; font-weight:bold;" value="<?php  ?>" />
		</div>
		</div>
		
		<div class="control-group">
		<label class="control-label" for="inputEmail" style="font-family:Lobaster; font-weight:bold; font-size:18px; color:blue;">User ID</label>
		<div class="controls">

			<input name="userid" required="required" type="text" style="font-family:Lobaster; font-weight:bold;" value="<?php   ?>" />
		</div>
		</div>
		
		<div class="control-group">
		<label class="control-label" for="inputEmail" style="font-family:Lobaster; font-weight:bold; font-size:18px; color:blue;">Password</label>
		<div class="controls">
			<input name="password" required="required" type="text" style="font-family:Lobaster; font-weight:bold;" value="<?php  ?>" />
		</div>
		</div>
	
		<div class="control-group">
		<label class="control-label" for="inputEmail" style="font-family:Lobaster; font-weight:bold; font-size:18px; color:blue;">Campus</label>
		<div class="controls">
			<select name="campus" id="select" required >
			<option value="">Select</option>
			<option value="JSPMNTC">JSPMNTC</option>
			<option value="TSSM">TSSM</option>
			<option value="Diploma">Diploma</option>
			<option value="Hostel">Hostel</option>
			<option value="Other">Other</option>
			<option value="Outside Campus">Outside Campus</option>
			</select>
		</div>
		</div>
		
		<div class="control-group">
		<label class="control-label" for="inputEmail" style="font-family:Lobaster; font-weight:bold; font-size:18px; color:blue;">Department</label>
		<div class="controls">
			<select name="department" id="select" required>
			<option value="">Select</option>
			<option value="First Year Eng">First Year Eng..</option>
			<option value="Computer Department">Computer Department</option>
			<option value="Civil Department">Civil Department</option>
			<option value="Mechanical Department">Mechanical Department</option>
			<option value="E nd TC Department">E & TC Department</option>
			<option value="Electrical Department">Electrical Department</option>
			<option value="MBA Department">MBA Department</option>
			<option value="MCA Department">MCA Department</option>
			<option value="Cignet School">Cignet School</option>
			<option value="Blossam School">Blossam School</option>
			<option value="Libary">Libary</option>
			<option value="Office">Office</option>
			<option value="Girls Hostel">Girls Hostel</option>
			<option value="Boys Hostel">Boys Hostel</option>
			<option value="Other">Other</option>
			<option value="Outside Campus">Outside Campus</option>
			</select>
		</div>
		</div>
		
		<div class="control-group">
		<label class="control-label" for="inputEmail" style="font-family:Lobaster; font-weight:bold; font-size:18px; color:blue;">Access</label>
		<div class="controls">
			<select name="access" id="select" required >
			<option value="">Select</option>
			<option value="gate">Gate User</option>
			<option value="store">Store User</option>
			<option value="guest">Requisition</option>
			</select>
		</div>
		</div>
		
	</div>
	</br>
	<input name="done" class="btn btn-success" style="margin-left: 165px; font-family:Lobaster;" type="submit" value="Add User"> 	 
	</br></br>
	</div>

	<br />	





</form>
</div>
</body>
</html>
