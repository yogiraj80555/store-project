 <?php

session_start();

if(empty($_SESSION['type']))
{
	header("location:../index.php");
}
if(!$_SESSION['type'] == "admin")
{
	header("location:../index.php");
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Store</title>
<style>
 body{
            background-color: #001110;
        }

.button {
    background-color: #4CAF50; /* Green */
    border: none;
    color: white;
    padding: 16px 32px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    margin: 4px 2px;
    -webkit-transition-duration: 0.4s; /* Safari */
    transition-duration: 0.4s;
    cursor: pointer;
	border-radius: 6px;
	width:300px;
}

.button1 {
    background-color: white; 
    color: black; 
    border: 2px solid #4CAF50;
}

.button1:hover {
    background-color: #4CAF50;
    color: white;
	font-style: oblique;
	font-variant:small-caps;
}

.button2 {
    background-color: white; 
    color: black; 
    border: 2px solid #008CBA;
}

.button2:hover {
    background-color: #008CBA;
    color: white;
	font-style: oblique;
	font-variant:small-caps;
}

.button3 {
    background-color: white; 
    color: black; 
    border: 2px solid #f44336;
}

.button3:hover {
    background-color: #f44336;
    color: white;
	font-style: oblique;
	font-variant:small-caps;
}

.button4 {
    background-color: white;
    color: black;
    border: 2px solid #000066;
}

.button4:hover {background-color: #000066;color: white; font-variant:small-caps; font-style: oblique;}

.button5 {
    background-color: white;
    color: black;
    border: 2px solid #555555;
}

.button5:hover {
    background-color: #555555;
    color: white;
	font-style: oblique;
	font-variant:small-caps;
}
.button6 {
    background-color: white;
    color: black;
    border: 2px solid #0033FF;
}

.button6:hover {
    background-color: #0033FF;
    color: white;
	font-style: oblique;
	font-variant:small-caps;
}

.button7 {
    background-color: white;
    color: black;
    border: 2px solid #996996;
}

.button7:hover {
    background-color: #996996;
    color: white;
	font-style: oblique;
	font-variant:small-caps;
}

.button8 {
    background-color: white;
    color: black;
    border: 2px solid  #99FF33;
}

.button8:hover {
    background-color: #99FF33;
    color: white;
	font-style: oblique;
	font-variant:small-caps;
}

.button9 {
    background-color: white;
    color: black;
    border: 2px solid  #123233;
}

.button9:hover {
    background-color: #123233;
    color: white;
	font-style: oblique;
	font-variant:small-caps;
	
}
.contain
{
margin-top:20%;
}
</style>
</head>

<body background="img/back2.jpg">

<div align="center" class="contain">
<a  href="plumbing.php"><button class="button button1"><strong>Plumbing Material</strong></button></a>
<a  href="cleaning.php"><button class="button button2">Cleaning Material</button></a>
<a  href="stationary.php"><button class="button button3"><strong>Stationary stock</strong></button></a><br />
<a  href="electrical.php"><button class="button button4">Electrical Material</button></a>
<a  href="printing.php"><button class="button button5"><strong>Printing Stationary</strong></button></a>
<a  href="computer.php"><button class="button button6">Computer Material</button></a>
<a  href="register.php"><button class="button button7"><strong>Register's</strong></button></a>
<a  href="manual.php"><button class="button button8">Manuals</button></a>
<a  href="other.php"><button class="button button9"><strong>Other Material</strong></button></a>
<a  href="civil.php"><button class="button button2"><strong>Civil Material</strong></button></a>
<a  href="user.php"><button class="button button5"><strong>Add Users</strong></button></a>
<a  href="../graph"><button class="button button1"><strong>Graph</strong></button></a>
<a  href="../logout.php"><button class="button button5"><strong>Logout</strong></button></a>
</div>
</body>
</html>
