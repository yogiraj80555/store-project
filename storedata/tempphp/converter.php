
<?php

?>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="/w3css/4/w3.css">
<style>
/* The Modal (background) */
.modal {
    display: none; /* Hidden by default */
    position:absolute; /* Stay in place */
    z-index: 1; /* Sit on top */
    padding-top: 100px; /* Location of the box */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}
.button {
     margin:10% 30% 30% 30% ;
    background-color: #4CAF50; /* Green */
    border: none;
	width:500px;height:300px;
    color: white;
    padding: 15px 32px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 56px;
}
/* Modal Content */
.modal-content {
    position: relative;
    background-color: #fefefe;
    margin: auto;
    padding: 0;
    border: 1px solid #888;
    width: 80%;
    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19);
    -webkit-animation-name: animatetop;
    -webkit-animation-duration: 0.4s;
    animation-name: animatetop;
    animation-duration: 0.4s
}

/* Add Animation */
@-webkit-keyframes animatetop {
    from {top:-300px; opacity:0} 
    to {top:0; opacity:1}
}

@keyframes animatetop {
    from {top:-300px; opacity:0}
    to {top:0; opacity:1}
}

/* The Close Button */
.close {
    color: white;
    float: right;
    font-size: 28px;
    font-weight: bold;
}

.close:hover,
.close:focus {
    color: #000;
    text-decoration: none;
    cursor: pointer;
}

.modal-header {
    padding: 1px 16px;
    background-color: #5cb85c;
    color: white;
}

.modal-body {padding: 2px 6px;}

.modal-footer {
    padding: 2px 6px;
    background-color: #5cb85c;
    color: white;
}
</style>
</head>
<body background="back2.jpg">


<!-- Trigger/Open The Modal -->
<button class="button" id="myBtn">Converter</button>

<!-- The Modal -->
<div id="myModal" class="modal">

  <!-- Modal content -->
  <div class="modal-content">
    <div class="modal-header">
      <span class="close">&times;</span>
      <h2>Converter</h2>
    </div>
    <div class="modal-body">
	 <div class="w3-row-padding" style="margin:0 -16px">

<table align="center" width="800" border="0" style="margin:20px auto">
  <tr>
    <td>  <div class="w3-half w3-margin-top">
    <label>Feet</label>
    <input id="inputFeet" class="w3-input w3-border" type="number" placeholder="Feet" oninput="lengthConverter(this.id,this.value)" onChange="lengthConverter(this.id,this.value)">
  </div></td>
    <td><div class="w3-half w3-margin-top">
    <label>Meters</label>
    <input id="inputMeters" class="w3-input w3-border" type="number" placeholder="Meters" oninput="lengthConverter(this.id,this.value)" onChange="lengthConverter(this.id,this.value)">
  </div></td>
    <td>  <div class="w3-half w3-margin-top">
    <label>Inches</label>
    <input id="inputInches" class="w3-input w3-border" type="number" placeholder="Inches" oninput="lengthConverter(this.id,this.value)" onChange="lengthConverter(this.id,this.value)">
  </div></td>
    <td><div class="w3-half w3-margin-top">
    <label>cm</label>
    <input id="inputcm" class="w3-input w3-border" type="number" placeholder="cm" oninput="lengthConverter(this.id,this.value)" onChange="lengthConverter(this.id,this.value)">
  </div></td>

  </tr>
  <tr>


  
  </tr>
</table>



  

  
 
 
</div>
	 
    </div>
  
  </div>

</div>

<script>
// Get the modal
var modal = document.getElementById('myModal');

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal 
btn.onclick = function() {
    modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}
</script>


<script>
function lengthConverter(source,valNum) {
  valNum = parseFloat(valNum);
  var inputFeet = document.getElementById("inputFeet");
  var inputMeters = document.getElementById("inputMeters");
  var inputInches = document.getElementById("inputInches");
  var inputcm = document.getElementById("inputcm");
  var inputYards = document.getElementById("inputYards");
  var inputKilometers = document.getElementById("inputKilometers");
  var inputMiles = document.getElementById("inputMiles");
  if (source=="inputFeet") {
    inputMeters.value=(valNum/3.2808).toFixed(2);
    inputInches.value=(valNum*12).toFixed(2);
    inputcm.value=(valNum/0.032808).toFixed();
    inputYards.value=(valNum*0.33333).toFixed(2);
    inputKilometers.value=(valNum/3280.8).toFixed(5);    
    inputMiles.value=(valNum*0.00018939).toFixed(5);
  }
  if (source=="inputMeters") {
    inputFeet.value=(valNum*3.2808).toFixed(2);
    inputInches.value=(valNum*39.370).toFixed(2);
    inputcm.value=(valNum/0.01).toFixed();
    inputYards.value=(valNum*1.0936).toFixed(2);
    inputKilometers.value=(valNum/1000).toFixed(5);    
    inputMiles.value=(valNum*0.00062137).toFixed(5);
  }
  if (source=="inputInches") {
    inputFeet.value=(valNum*0.083333).toFixed(3);
    inputMeters.value=(valNum/39.370).toFixed(3);
    inputcm.value=(valNum/0.39370).toFixed(2);
    inputYards.value=(valNum*0.027778).toFixed(3);    
    inputKilometers.value=(valNum/39370).toFixed(6);
    inputMiles.value=(valNum*0.000015783).toFixed(6);
  }
  if (source=="inputcm") {
    inputFeet.value=(valNum*0.032808).toFixed(3);
    inputMeters.value=(valNum/100).toFixed(3);
    inputInches.value=(valNum*0.39370).toFixed(2);
    inputYards.value=(valNum*0.010936).toFixed(3);    
    inputKilometers.value=(valNum/100000).toFixed(6);
    inputMiles.value=(valNum*0.0000062137).toFixed(6);
  }
  if (source=="inputYards") {
    inputFeet.value=(valNum*3).toFixed();
    inputMeters.value=(valNum/1.0936).toFixed(2);
    inputInches.value=(valNum*36).toFixed();
    inputcm.value=(valNum/0.010936).toFixed();
    inputKilometers.value=(valNum/1093.6).toFixed(5);
    inputMiles.value=(valNum*0.00056818).toFixed(5);
  }
  if (source=="inputKilometers") {
    inputFeet.value=(valNum*3280.8).toFixed();
    inputMeters.value=(valNum*1000).toFixed();
    inputInches.value=(valNum*39370).toFixed();
    inputcm.value=(valNum*100000).toFixed();
    inputYards.value=(valNum*1093.6).toFixed();
    inputMiles.value=(valNum*0.62137).toFixed(2);    
  }
  if (source=="inputMiles") {
    inputFeet.value=(valNum*5280).toFixed();
    inputMeters.value=(valNum/0.00062137).toFixed();
    inputInches.value=(valNum*63360).toFixed();
    inputcm.value=(valNum/0.0000062137).toFixed();
    inputYards.value=(valNum*1760).toFixed();
    inputKilometers.value=(valNum/0.62137).toFixed(2);    
  }
}
</script>

</body>
</html>
