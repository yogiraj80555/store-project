<?php
session_start();
include('dbcon.php');
require_once 'include/DbO.php';
$db = new DbOperations();
if(empty($_SESSION['type']))
{
	header("location:index.php");
}
if(!$_SESSION['type'] == "gate")
{
	header("location:index.php");//hear store 1st page
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta http-equiv="refresh" content="90">
<title>Store</title>
<style>
.button {
    background-color: #4CAF50; /* Green */
    border: none;
    color: white;
    padding: 16px 32px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    margin: 4px 2px;
    -webkit-transition-duration: 0.4s; /* Safari */
    transition-duration: 0.4s;
    cursor: pointer;
	border-radius: 6px;
	width:300px;
}

.button1 {
    background-color: white; 
    color: black; 
    border: 2px solid #4CAF50;
}

.button1:hover {
    background-color: #4CAF50;
    color: white;
}

.button2 {
    background-color: white; 
    color: black; 
    border: 2px solid #008CBA;
}

.button2:hover {
    background-color: #008CBA;
    color: white;
}

.button3 {
    background-color: white; 
    color: black; 
    border: 2px solid #f44336;
}

.button3:hover {
    background-color: #f44336;
    color: white;
}

.button4 {
    background-color: white;
    color: black;
    border: 2px solid #000066;
}

.button4:hover {background-color: #000066;color: white;}

.button5 {
    background-color: white;
    color: black;
    border: 2px solid #555555;
}

.button5:hover {
    background-color: #555555;
    color: white;
}
.containqw
{
margin-top:10%;
}
#notification-count{
	
	left: 0px;
	top: 0px;
	font-size: 1.0em;		
	color: #de5050;
	font-weight:bold;
}
</style>
</head>

<body background="img/back2.jpg">
<?php include("menu.php"); 
 ?>
<div align="center" class="containqw">

<a href="gate/gate1.php"><button class="button button5">Store Material</button></a>
<a href="tempdata/"><button class="button button1">Temporary Material</button></a><br />
<a href="report.php"><button class="button button3">Report</button></a><br />
</div>
</body>
</html>
